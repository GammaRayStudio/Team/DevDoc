團隊開發手冊 
=====


<br>

簡單規則
------

	新專案依循此規則，舊專案看情況。
	不能參照沒關係，盡量符合就行，以不破壞原代碼機制的原則下開發。

<br>

### 代碼風格
`coding style`

+ <https://gitlab.com/GammaRayStudio/DevDoc/-/blob/master/CodingStyle/001.JavaStyleGuide.md?ref_type=heads>

<br>

### 版本控制
`git / gitlab`

**git-flow**

+ <https://gitlab.com/GammaRayStudio/DevDoc/-/blob/master/Git/001-4.git-flow.md?ref_type=heads>

**git commit message**

+ <https://gitlab.com/GammaRayStudio/DevDoc/-/blob/master/Git/001-5.git-commit-message.md?ref_type=heads>

<br>

### 文檔風格
`doc style`

+ <https://gitlab.com/GammaRayStudio/DevDoc/-/blob/master/Markdown/002.markdown-tutor.md?ref_type=heads>

<br>

### 專案目錄
`DDD , Domain Driven Desgin , 領域驅動設計`

+ <https://gitlab.com/GammaRayStudio/DevDoc/-/blob/master/Design/004.%E9%A0%98%E5%9F%9F%E9%A9%85%E5%8B%95%E8%A8%AD%E8%A8%88.md?ref_type=heads>

<br>

### 測試先行
`TDD , Test Driven Design , 測試驅動開發`

<https://gitlab.com/GammaRayStudio/DevDoc/-/blob/master/Programmer/004.%E6%B8%AC%E8%A9%A6%E9%A9%85%E5%8B%95%E9%96%8B%E7%99%BC.md?ref_type=heads>

<br>

準備環境
------
`本機電腦`

+ IntelliJ IDEA 2021.1.3 企業版 (不要載最新版)
+ Java 8
+ MySQL 8
	+ 介面操作工具 (ex : Dbeaver)
	+ 準備新增使用者指令 (sql : CREATE USER ... )
+ Redis 7
	+ 介面操作工具 (ex : Redis Desktop Manager , RedisInsight)
+ Docker
	+ 不限版本，不一定會用到
+ Tomcat : 8.5.69
+ node.js : v10.3.0
  + npm : 6.1.0
+ FileZilla : FTP Client
+ Dbeaver : SQL Editor

<br>

準備帳號
------
+ Jira
+ Gitlab
+ Telegram





